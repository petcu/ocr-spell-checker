import re
import os
import glob

source = "./txt/"
destination = "./cuvinte/"

if __name__ == "__main__":
    dictionare = glob.glob(source + "*.txt")
    if not os.path.isdir(destination):
        os.makedirs(destination)
    for dictionar in dictionare:
        content = open(dictionar,"r").readlines()
        output = open(dictionar.replace(source,destination),"w")
        for line in content:
            line = re.sub(r"(:|-).+","",line)
            # Eliminare caractere unicode din blocul latin-B
            line = re.sub("ș","ş",line)
            line = re.sub("ț","ţ",line)
            if len(line) > 1:
                output.write(line)