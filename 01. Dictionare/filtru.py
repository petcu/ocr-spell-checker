import os
import glob

source = "./cuvinte"
destination = "./dictionar.txt"

if __name__ == '__main__':
    dictionare = glob.glob(f"{source}/*.txt")
    words = []
    for dictionar in dictionare:
        content = open(dictionar,"r").readlines()
        for line in content:
            words.append(line)
    dictionary = set(words)
    file_out = open(destination,"w")
    for word in dictionary:
        file_out.write(word)
    file_out.close()