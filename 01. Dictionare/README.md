# Dicionare

[TOC]

## Download Dicţionare în format StarDict

### [Dexonline - Unelte](https://dexonline.ro/unelte)

- [DEX 2009](https://www.dropbox.com/sh/wcwy218thme2wm4/AAASdxnPdCLJ0jNa2hfR6_AWa?dl=1)
- [MDN 2000 + 2008](https://www.dropbox.com/sh/gbm1ka3xhoh0og1/AABzd0Fc2aOkq6EYHjR8GYtga?dl=1)
- [DLRLC 1955-1957](https://www.dropbox.com/sh/4e3djhod4fznyfw/AABPaQ9w8NlDnx5hHnvJfUwUa?dl=1)
- [Șăineanu 1929](https://www.dropbox.com/sh/2dd7i4px3yvpyjm/AABas5FASZx5T7JlV6zQCsYza?dl=1)
- [Toate cele de mai sus](https://www.dropbox.com/sh/57vuu2axdtg9jr2/AAAf8WcKbCftc8dFpB_21Ok_a?dl=1)

## Conversie StarDict to TXT

Plasează fişierele .idx, .ifo, .dict.dz în folderul sursă.

**Sursă**: ./source
**Destinaţie**: ./txt

### [Script - Github](https://gist.github.com/rongyi/ff2f8a1a82cddb2efc9239bb0d7ca78b)

```python
# Modificări
if __name__ == '__main__':
    source = "./source/"
    destination = "./txt/"
    dictionare = glob.glob(source + "*.ifo")
    for dictionar in dictionare:
        file_name = dictionar[:-4]
        ifo_file = file_name + ".ifo"
        idx_file = file_name + ".idx"
        dict_file = file_name + ".dict.dz"
        # info read test done
        info = IfoFileReader(ifo_file)
        # index read test
        index = IdxFileReader(idx_file)
        # dict test
        dict_reader = DictFileReader(dict_file, info, index, True)
        if not os.path.isdir(destination):
            os.makedirs(destination)
        dict_reader.dump(file_name.replace(source,destination) + ".txt")
```

**Rurare**

```bash
> python2 stardict2txt.py
```

## Extragere Cuvinte din Dicţionar

**Sursă**: "./txt"
**Destinaţie**: "./cuvinte"

**Script**

```python
import re
import os
import glob

source = "./txt/"
destination = "./cuvinte/"

if __name__ == "__main__":
    dictionare = glob.glob(source + "*.txt")
    if not os.path.isdir(destination):
        os.makedirs(destination)
    for dictionar in dictionare:
        content = open(dictionar,"r").readlines()
        output = open(dictionar.replace(source,destination),"w")
        for line in content:
            line = re.sub(r"(:|-).+","",line)
            # Eliminare caractere unicode din blocul latin-B
            line = re.sub("ș","ş",line)
            line = re.sub("ț","ţ",line)
            if len(line) > 1:
                output.write(line)
```

**Rurare**

```bash
> python3 extrage.py
```

## Combinare şi eliminare duplicate

**Sursă**: "./cuvinte"
**Destinaţie**: "./dictionar.txt"

**Script**

```python
import os
import glob

source = "./cuvinte"
destination = "./dictionar.txt"

if __name__ == '__main__':
    dictionare = glob.glob(f"{source}/*.txt")
    words = []
    for dictionar in dictionare:
        content = open(dictionar,"r").readlines()
        for line in content:
            words.append(line)
    dictionary = set(words)
    file_out = open(destination,"w")
    for word in dictionary:
        file_out.write(word)
    file_out.close()
```

**Rurare**

```bash
> python3 filtru.py
```

