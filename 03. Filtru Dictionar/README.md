 # Filtru

[TOC]

## Filtrare cuvinte din Wikipedia

**Sursă dicţionar**: "./dictionar.txt"
**Sursă wiki count**: "./wiki.txt"
**Destinaţie count**: "./words.txt"
**Destinaţie erori**: "./errors.txt"

```python
from collections import Counter

sursa_dictionar = "dictionar.txt"
sursa_wiki = "wiki.txt"
destinatie = "words.txt"
errors = "errors.txt"

if __name__ == '__main__':
    words = []
    content = open(sursa_dictionar,"r").readlines()
    for line in content:
        words.append(line[:-1])
    dictionary = set(words)

    date_in = open(sursa_wiki,"r").readlines()
    date_out = open(destinatie,"w")
    erori = open(errors,"w")

    for line in date_in:
        text = line.split()
        if text[0] in dictionary:
            date_out.write(line)
        else:
            erori.write(line)
    date_out.close()
    erori.close()
```

**Rulare**

```bash
> python3 filtru.py
```

## (Opţional) Creare Dictionar Autocorect (Py)

```bash
# Creare envinronment
> python3 -m venv venv
# Activare envinronment
> source /venv/bin/activate
# Install autocorrect
> pip install autocorrect
```

**Adaugă în /venv/lib/python3.8/site-packages/autocorrect/constants.py**

```python
word_regexes = {
    .....
    'ro': r'[a-zA-ZşŞţŢăĂâÂîÎțȚșȘǎǍȃȂȋȊ]+',
}
alphabets = {
    .....
    'ro': 'abcdefghijklmnopqrstuvwxyzşŞţŢăĂâÂîÎțȚșȘǎǍȃȂȋȊ',
}
```

**Generare dicţionar**

```bash
> tar -zcvf a/venv/lib/python3.8/site-packages/autocorrect/data/ro.tar.gz wiki.json
```