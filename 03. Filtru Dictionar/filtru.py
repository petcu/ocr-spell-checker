from collections import Counter

sursa_dictionar = "dictionar.txt"
sursa_wiki = "wiki.txt"
destinatie = "words.txt"
errors = "errors.txt"

if __name__ == '__main__':
    words = []
    content = open(sursa_dictionar,"r").readlines()
    for line in content:
        words.append(line[:-1])
    dictionary = set(words)

    date_in = open(sursa_wiki,"r").readlines()
    date_out = open(destinatie,"w")
    erori = open(errors,"w")

    for line in date_in:
        text = line.split()
        if text[0] in dictionary:
            date_out.write(line)
        else:
            erori.write(line)
    date_out.close()
    erori.close()