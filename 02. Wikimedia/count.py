import os
import re
import glob
from collections import Counter

def prepare_text(text):
    # Remove <doc> tag
    text = re.sub("<(\/|)doc.+", "", text)
    # Remove links
    text = re.sub("(http|https|www).+", "", text)
    # Make everything Lowercase
    text = text.lower()
    # Replace Latin-B Characters
    text = re.sub("ț","ţ",text)
    text = re.sub("ș","ş", text)
    return text

def get_files(path_parent):
    files = []
    folders = [f for f in glob.glob(f"{path_parent}/**/")]
    for folder in folders:
        files += [f for f in glob.glob(f"{folder}/*")]
    return files

def create_file(path, text):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path,"w") as f:
        f.write(text)

def get_words(text):
    reg = r"[a-zA-ZşŞţŢăĂâÂîÎ]{2,}"
    text = prepare_text(text)
    words = re.findall(reg, text)
    return words

def get_txt():
    text_out = ""
    word_list = word_count.most_common()
    for word in word_list:
        text_out += f"{word[0]} {word[1]}\n"
    return text_out

def get_json():
    text_out = "{\n"
    word_list = word_count.most_common()
    for word in word_list:
        text_out += f"\"{word[0]}\": {word[1]},\n"
    text_out += "}\n"
    return text_out

if __name__ == '__main__':
    # Place
    path_parent = 'text'
    path_destination = 'wiki.txt'
    # wiki.json
    
    files = get_files(path_parent)
    word_count = Counter()

    for file in files:
        f = open(file,"r")
        text = f.read()
        word_count.update(get_words(text))
        f.close()

    file_out = open(path_destination,"w")
    file_out.write(get_txt())
    # get_json() pentru json
    file_out.close()
